/*
 * @Author: Yinjie Lee
 * @Date: 2022-05-03 09:47:32
 * @LastEditors: Yinjie Lee
 * @LastEditTime: 2022-05-03 15:22:47
 */
#ifndef CALLBACK_H
#define CALLBACK_H
typedef struct _context
{
    int id;
    int status;
}context;


typedef void* (*__call_fread)(int,char*,void*);
static void* call_fread(void* _cb_Func,int len ,char* buf,int status,int id){
    context ct;
    ct.id = id;
    ct.status = status;
    return ((__call_fread)_cb_Func)(len,buf,&ct);
}
#endif//CALLBACK_H
