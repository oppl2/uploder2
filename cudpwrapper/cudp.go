/*
 * @Author: Yinjie Lee
 * @Date: 2022-05-03 23:52:12
 * @LastEditors: Yinjie Lee
 * @LastEditTime: 2022-05-04 13:56:20
 */
package main

// #include <stdio.h>
// #include <stdlib.h>
// #include <stdint.h>
// #include <string.h>
// #include <pthread.h>
// #include "callback.h"
// #define HANDLE_SIZE 1024
import "C"
import (
	"log"
	"time"
	"unsafe"
	"uploder2/udpwrapper"
)

//export ConnUDPServer
func ConnUDPServer(server *C.char, handle *C.char) {
	serverBytes := C.GoString(server)
	h := udpwrapper.UDP_connect(serverBytes)
	uuid := h.GetUUID()
	p := unsafe.Pointer(C.CString(uuid))
	defer C.free(p)
	C.memcpy(unsafe.Pointer(handle), p, C.ulong(len(uuid)))
}

//export ConnClose
func ConnClose(handle *C.char) {
	h := udpwrapper.GetHandle(C.GoString(handle))
	h.UDP_close()
}

//export UDPSwap
func UDPSwap(handle *C.char, source *C.char, slen C.int, recvlen C.int, taskID C.int, call_back unsafe.Pointer) {
	if source == nil {
		log.Printf("Please check buffer pointer,source=%p", source)
	}
	h := udpwrapper.GetHandle(C.GoString(handle))
	sourceBytes := C.GoBytes(unsafe.Pointer(source), slen)
	ret := h.UDP_swap(int(taskID), sourceBytes, int(recvlen))
	if len(ret) <= 0 {
		C.call_fread(call_back, 0, nil, C.int(udpwrapper.FAILED), taskID)
		log.Printf("udp swap , read buf is nil")
		return
	}
	p := unsafe.Pointer(&ret[0])
	C.call_fread(call_back, C.int(len(ret)), (*C.char)(p), C.int(udpwrapper.SUCCESS), taskID)
}

//export UDPSwapDoBest
func UDPSwapDoBest(handle *C.char, source *C.char, slen C.int, recvlen C.int, taskID C.int, call_back unsafe.Pointer) {
	if source == nil {
		log.Printf("Please check buffer pointer,source=%p", source)
	}
	h := udpwrapper.GetHandle(C.GoString(handle))
	sourceBytes := C.GoBytes(unsafe.Pointer(source), slen)
	ret := h.UDP_swap_new(int(taskID), sourceBytes, int(recvlen))
	if len(ret) <= 0 {
		C.call_fread(call_back, 0, nil, C.int(udpwrapper.FAILED), taskID)
		log.Printf("udp swap , read buf is nil")
		return
	}
	p := unsafe.Pointer(&ret[0])
	C.call_fread(call_back, C.int(len(ret)), (*C.char)(p), C.int(udpwrapper.SUCCESS), taskID)
}

//export GetRemoteAddr
func GetRemoteAddr(handle *C.char, server *C.char) {
	if server == nil {
		log.Printf("Please check buffer pointer,server=%p", server)
	}
	h := udpwrapper.GetHandle(C.GoString(handle))
	s := h.UDP_ServerAddr()
	p := unsafe.Pointer(C.CString(s))
	defer C.free(p)
	C.memcpy(unsafe.Pointer(server), p, C.ulong(len(s)))
}

//export UDPSetDeadline
func UDPSetDeadline(handle *C.char, second C.int) {
	h := udpwrapper.GetHandle(C.GoString(handle))
	h.UDP_SetDeadline(time.Second * time.Duration(second))
}

//export UDPSetReadDeadline
func UDPSetReadDeadline(handle *C.char, second C.int) {
	h := udpwrapper.GetHandle(C.GoString(handle))
	h.UDP_SetReadDeadline(time.Second * time.Duration(second))
}

//export UDPSetWriteDeadline
func UDPSetWriteDeadline(handle *C.char, second C.int) {
	h := udpwrapper.GetHandle(C.GoString(handle))
	h.UDP_SetWriteDeadline(time.Second * time.Duration(second))
}

func main() {
	log.Println("hello world")
}
