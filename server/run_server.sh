#!/bin/bash
###
 # @Author: Yinjie Lee
 # @Date: 2022-05-04 01:02:38
 # @LastEditors: Yinjie Lee
 # @LastEditTime: 2022-05-04 01:05:10
###
go run server.go -port 49996 -udp -reuseport -loops 4