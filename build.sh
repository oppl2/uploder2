#!/bin/bash
###
 # @Author: Yinjie Lee
 # @Date: 2022-05-03 11:33:57
 # @LastEditors: Yinjie Lee
 # @LastEditTime: 2022-05-04 12:40:50
###

echo "下载go 依赖包  ..."
go mod init uploder2
go mod tidy
echo "cudpwrapper 目录  ..."
cd cudpwrapper
LIBRARY_NAME="libudpuploder2"
echo "设置 go 动态库名称为 ${LIBRARY_NAME}  ..."
echo "生成go 动态库${LIBRARY_NAME}  ..."
go build -buildmode=c-shared -o "${LIBRARY_NAME}".so
echo "拷贝头文件与动态库到 c_demo 目录  ..."
cp "${LIBRARY_NAME}".* callback.h ../c_demo

cd ..
cd c_demo
echo "生成c语言测试工程  ..."
gcc test.c -ludpuploder2 -lpthread -L. -o test
cd ..
echo "Done   "

