/*
 * @Author: Yinjie Lee
 * @Date: 2022-05-03 21:36:58
 * @LastEditors: Yinjie Lee
 * @LastEditTime: 2022-05-04 14:17:28
 */
package udpwrapper

import (
	"log"
	"net"
	"reflect"
	"sync"
	"time"
	parseaddr "uploder2/parserAddr"

	"github.com/fatih/pool"
	"github.com/google/uuid"
)

const (
	FAILED  = -1
	SUCCESS = 0
	EXIST   = 1
)

type Handle struct {
	uuid             string
	server           string
	factory_udp      pool.Pool
	port             net.Conn
	readDeadline     time.Duration
	readDeaTrigger   bool
	writeDeadline    time.Duration
	writeDeaTrigger  bool
	commonDeadline   time.Duration
	commonDeaTrigger bool
}

var global_handle sync.Map

func UDP_connect(server string) *Handle {
	h := new(Handle)
	uuid := uuid.New()
	h.uuid = uuid.String()
	h.server = server
	network, addr, _, _ := parseaddr.ParseAddr(server)
	factory := func() (net.Conn, error) { return net.Dial(network, addr) }
	p, err := pool.NewChannelPool(5, 50, factory)
	if err != nil {
		panic("UDP pool 创建失败！")
	}
	h.factory_udp = p
	port, err := p.Get()
	if err != nil {
		log.Println("Error: get UDP conn failed !", err)
	}
	h.port = port
	global_handle.Store(h.uuid, h)
	return h
}

func (h *Handle) UDP_swap(id int, buf []byte, recvlen int) (ret []byte) {
	var wg sync.WaitGroup
	conn := h.GetDefaultUDPConn()
	udprd := func() {
		msg := make([]byte, recvlen)
		l, err := conn.Read(msg)
		if err != nil {
			log.Println("Write buf to server failed! id =", id, err)
		}
		ret = msg[:l]
		defer wg.Done()
	}
	udprt := func() {
		_, err := conn.Write(buf)
		if err != nil {
			log.Println("Write buf to server failed! id =", id, err)
		}
		defer wg.Done()
	}
	wg.Add(1)
	go udprt()
	wg.Add(1)
	go udprd()
	wg.Wait()
	return ret
}

func (h *Handle) UDP_swap_new(id int, buf []byte, recvlen int) (ret []byte) {
	var wg sync.WaitGroup
	conn := h.GetUDPConn()
	defer conn.Close()
	udprd := func() {
		msg := make([]byte, recvlen)
		l, err := conn.Read(msg)
		if err != nil {
			log.Println("Write buf to server failed! id =", id, err)
		}
		ret = msg[:l]
		defer wg.Done()
	}
	udprt := func() {
		_, err := conn.Write(buf)
		if err != nil {
			log.Println("Write buf to server failed! id =", id, err)
		}
		defer wg.Done()
	}
	wg.Add(1)
	go udprt()
	wg.Add(1)
	go udprd()
	wg.Wait()
	return ret
}

func (h *Handle) UDP_close() {
	h.factory_udp.Close()
}

func (h *Handle) UDP_ServerAddr() string {
	return h.server
}

func (h *Handle) UDP_SetDeadline(ti time.Duration) {
	h.commonDeadline = ti
	h.commonDeaTrigger = true
	h.writeDeaTrigger = false
	h.readDeaTrigger = false
}

func (h *Handle) UDP_SetReadDeadline(ti time.Duration) {
	h.readDeadline = ti
	h.commonDeaTrigger = false
	h.writeDeaTrigger = false
	h.readDeaTrigger = true
}

func (h *Handle) UDP_SetWriteDeadline(ti time.Duration) {
	h.writeDeadline = ti
	h.commonDeaTrigger = false
	h.writeDeaTrigger = true
	h.readDeaTrigger = false
}

func (h *Handle) GetUDPConn() net.Conn {
	conn, err := h.factory_udp.Get()
	if err != nil {
		log.Println("Get UDP conn from UDP pool failed! ", err)
		return nil
	}
	switch true {
	case h.commonDeaTrigger:
		if err := conn.SetDeadline(time.Now().Add(h.commonDeadline)); err != nil {
			log.Println("UDP SetDeadline failed! at handler uuid=", h.uuid)
		}
	case h.writeDeaTrigger:
		if err := conn.SetWriteDeadline(time.Now().Add(h.writeDeadline)); err != nil {
			log.Println("UDP SetWriteDeadline failed! at handler uuid=", h.uuid)
		}
	case h.readDeaTrigger:
		if err := conn.SetReadDeadline(time.Now().Add(h.readDeadline)); err != nil {
			log.Println("UDP SetReadDeadline failed! at handler uuid=", h.uuid)
		}
	}
	return conn
}

func (h *Handle) GetDefaultUDPConn() net.Conn {
	if h.port == nil {
		h.port = h.GetUDPConn()
	}
	conn := h.port
	switch true {
	case h.commonDeaTrigger:
		if err := conn.SetDeadline(time.Now().Add(h.commonDeadline)); err != nil {
			log.Println("UDP SetDeadline failed! at handler uuid=", h.uuid)
		}
	case h.writeDeaTrigger:
		if err := conn.SetWriteDeadline(time.Now().Add(h.writeDeadline)); err != nil {
			log.Println("UDP SetWriteDeadline failed! at handler uuid=", h.uuid)
		}
	case h.readDeaTrigger:
		if err := conn.SetReadDeadline(time.Now().Add(h.readDeadline)); err != nil {
			log.Println("UDP SetReadDeadline failed! at handler uuid=", h.uuid)
		}
	}
	return conn
}

func GetHandle(uuid string) *Handle {
	if h, ok := global_handle.Load(uuid); ok {
		h2, ok := h.(*Handle)
		if ok {
			return h2
		} else {
			log.Println("Oops, not found type ", reflect.TypeOf(h2))
		}
	}
	return nil
}

func (h *Handle) GetUUID() string {
	return h.uuid
}
