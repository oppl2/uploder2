/*
 * @Author: Yinjie Lee
 * @Date: 2022-05-03 22:24:52
 * @LastEditors: Yinjie Lee
 * @LastEditTime: 2022-05-04 02:17:32
 */
package main

import (
	"fmt"
	"strconv"
	"time"
	"uploder2/udpwrapper"
)

func elapsed(what string) func() {
	start := time.Now()
	return func() {
		fmt.Printf("%s took %v\n", what, time.Since(start))
	}
}
func main() {
	defer elapsed("page")()
	h := udpwrapper.UDP_connect("udp://127.0.0.1:49996")
	for i := 0; i < 10000; i++ {
		buf := "hellowold" + strconv.Itoa(i)
		h.UDP_swap(i, []byte(buf), 1024)
	}
	h.UDP_close()
}
