<!--
 * @Author: Yinjie Lee
 * @Date: 2022-05-04 02:27:31
 * @LastEditors: Yinjie Lee
 * @LastEditTime: 2022-05-04 14:02:09
-->
# uploder2 for UDP client

## API

```c
int callback_read(int buflength, char *buf,struct context *ct)
{
    if (ct == NULL || buf == NULL){
        printf("buf or ct is nil\n");
        return -1;
    }
    printf("context id = %d\n", ct->id);
    printf("context status = %d\n", ct->status);
    printf("recv len = %d\n", buflength);
    printf("recv str : %s \n", buf);
    return 1;
}

int main(){
    char *udp_addr = "udp://127.0.0.1:49996";
    char handle[HANDLE_SIZE] = {0};
    // 创建UDPServer，并返回一个handle，支持多线程调用
    ConnUDPServer(udp_addr,handle);
    //设置读取5s超时
    UDPSetReadDeadline(handle,5);
    //设置发送5s超时
    UDPSetWriteDeadline(handle,5);
    //设置发送读取5s超时
    UDPSetDeadline(handle,5);

    char buf[1024] = {0};
    // 设置接收缓冲区最大值
    int recvlen = 512;

    for(int thread_id =0;i < 2048;i++){
        sprintf(buf,"hello wrold %d\n",i);
        UDPSwap(handle,buf,strlen(buf),recvlen,thread_id,callback_read);
        // 基于UDP pool发包，会创建多个UDP 端口发送
        // UDPSwapDoBest(handle,buf,1024,recvlen,thread_id,callback_read);
    }
    // 关闭handle 的所有链接
    ConnClose(handle);
}
```

## 测试验证
```shell
cd server
#启动UDP server，其他UDP server 工具容易崩溃，尽量不用
./run_server.sh
cd ..
./build.sh
cd c_demo
./test
#测试代码请自助修改
```