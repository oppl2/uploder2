/*
 * @Author: Yinjie Lee
 * @Date: 2022-05-02 14:56:45
 * @LastEditors: Yinjie Lee
 * @LastEditTime: 2022-05-04 14:01:21
 */
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include "libudpuploder2.h"

#define NUM_THREADS 50
#define FAILED -1
#define SUCCESS 0
#define EXIST 1

struct context
{
    int id;
    int status;
};
struct thread_data
{
    int thread_id;
    void * handle;
};


int callback_read(int buflength, char *buf,struct context *ct)
{
    if (ct == NULL || buf == NULL){
        printf("上下文为空\n");
        return -1;
    }
    printf("context id = %d\n", ct->id);
    printf("context status = %d\n", ct->status);
    printf("recv len = %d\n", buflength);
    printf("recv str : %s \n", buf);
    return SUCCESS;
}

void *UDP_swap(void *threadarg)
{
    struct thread_data *my_data;
    my_data = (struct thread_data *)threadarg;
    char buf[1024] = {0};
    //模拟image 拆分成2048个包
    int recvlen = 1024;
    for(int i =0;i < 2048;i++){
        sprintf(buf,"hello wrold %d\n",i);
        // UDPSwap(my_data->handle,buf,1024,recv_buf,BUFLENGTH,my_data->thread_id);
        UDPSwapDoBest(my_data->handle,buf,strlen(buf),recvlen,my_data->thread_id,callback_read);
        // printf("task %d %s\n",my_data->thread_id);
    }
    // UDPSwap(my_data->handle,buf,1024,recv_buf,BUFLENGTH,my_data->thread_id);
    pthread_exit(NULL);
}

int main()
{
    pthread_t threads[NUM_THREADS];
    pthread_t threads_handler;

    struct thread_data td[NUM_THREADS];

    int rc, i;
    char *udp_addr = "udp://127.0.0.1:49996";

    char handle[HANDLE_SIZE] = {0};
    ConnUDPServer(udp_addr,handle);
    UDPSetReadDeadline(handle,5);

    for (i = 0; i < NUM_THREADS; i++)
    {
        printf("main() : creating thread, %d\n", i);
        struct context ct;

        td[i].thread_id = i;
        td[i].handle = handle;
        rc = pthread_create(&threads[i], NULL,
                            UDP_swap, (void *)&td[i]);

        if (rc)
        {
            printf("Error:unable to create thread, %d\n", rc);
            exit(-1);
        }
    }

    pthread_exit(NULL);
    printf("delete ConnClose\n");
    ConnClose(handle);
}