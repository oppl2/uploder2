module uploder2

go 1.18

require (
	github.com/fatih/pool v3.0.0+incompatible
	github.com/google/uuid v1.3.0
	github.com/tidwall/evio v1.0.8
)

require github.com/kavu/go_reuseport v1.5.0 // indirect
